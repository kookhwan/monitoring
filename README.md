# Project Title

Monitoring program using Queue

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

- .NET Framework 4.5.1 or higher
- Windows 7 or higher
- Visual Studio 2019
- MS SQL Server 2017
- Message Queuing

### Installing

- Create a database named "YoYoData" in MS SQL Server.
- Create a Private Queues named "yoyo" in Computer Management.

## Running the tests

- Execute YoYoSim.exe and click the start button.
- Execute Monitoring and Click the "Start Read" button.
- Choose a list of products, then you can see the graph and table of the product.

## Authors

- Kookhwan Im - Students of Computer Programming Analyst Student at Conestoga College

## License

- This project is licensed under the MIT License - see the [LICENSE.md] file for details
