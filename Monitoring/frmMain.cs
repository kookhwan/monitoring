﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Messaging;
using System.Windows.Forms.DataVisualization.Charting;

namespace Monitoring
{
    public partial class frmMain : Form
    {
        MessageQueue msmq = new MessageQueue();
        Boolean bRead = false;
        String queueName = "\\private$\\yoyo";  // Message queue root directory for connecting to a queue.
        DAL dal;

        public frmMain()
        {
            InitializeComponent();

            msmq.Formatter = new ActiveXMessageFormatter();
            msmq.MessageReadPropertyFilter.LookupId = true;
            msmq.SynchronizingObject = this;
            msmq.ReceiveCompleted += new ReceiveCompletedEventHandler(msmq_ReceiveCompleted);
            dal = new DAL();

            Create_Chart();
            Create_GridView();
            getData();
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            try
            {
                dal.CreateTable();  // Creation a table (YoYoDetails)
                txtQueueServer.Text = System.Windows.Forms.SystemInformation.ComputerName;
                IsRunning(false);
            }
            catch
            {
                MessageBox.Show("Can't create a table on Database as YoYoDetail" +
                Environment.NewLine + Environment.NewLine +
                "Please create a database as 'YoYoData' first");
                this.Close();
            }
        }


        /// <summary>
        /// Receive Queue massage and data from database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void msmq_ReceiveCompleted(object sender, ReceiveCompletedEventArgs e)
        {
            try
            {
                    WriteData(e.Message.Body.ToString());
                    getData();
                    lstQueueData.Items.Insert(0, e.Message.Body.ToString());
                    msmq.EndReceive(e.AsyncResult);

                    txtRemaining.Text = GetMessageCount(msmq).ToString();

                    Application.DoEvents();
                    if (bRead)
                    {
                        msmq.BeginReceive();
                    }
            }
            catch
            {
                MessageBox.Show("Unhandled Exception");
            }
        }

        /// <summary>
        /// Writing data to DB
        /// </summary>
        /// <param name="yoyoMessageBody"></param>
        private void WriteData(String yoyoMessageBody)
        {
            String[] yoyoData = yoyoMessageBody.Split(',');
            YoYoDataEntity yoyo = new YoYoDataEntity();
            yoyo.WorkArea = yoyoData[0];
            yoyo.SerialNumber = Guid.Parse(yoyoData[1]);
            yoyo.Line = yoyoData[2];
            yoyo.State = yoyoData[3];
            yoyo.Reason = yoyoData[4];
            yoyo.DTStamp = Convert.ToDateTime(yoyoData[5]);
            yoyo.ProductID = Convert.ToInt32(yoyoData[6]);
            dal.WriteToDB(yoyo);
        }

        /// <summary>
        /// Delete all record from DB
        /// </summary>
        private void RemoveData()
        {
            dal.DeleteToDB();
        }

        /// <summary>
        /// Getting data from database
        /// </summary>
        private void getData()
        {
            DataTable paretoTable = dal.GetFailCount();
            DataTable countTable = dal.GetTotalCount();

            chartPareto.DataSource = paretoTable;
            chartPareto.Refresh();

            int nMold = countTable.Rows[0].Field<int>(0);
            int nPaint = countTable.Rows[0].Field<int>(1);
            int nAssembly = countTable.Rows[0].Field<int>(2);
            int nPackage = countTable.Rows[0].Field<int>(3);

            dgvData.Rows[0].Cells[1].Value = nMold;
            dgvData.Rows[1].Cells[1].Value = nPaint;
            if(nPaint > 0 && nMold > 0)
            {
                dgvData.Rows[2].Cells[1].Value = ((nPaint * 1.0) / nMold).ToString("0.00 %");
            }
            dgvData.Rows[3].Cells[1].Value = nAssembly;
            if(nAssembly > 0 && nPaint > 0)
            {
                dgvData.Rows[4].Cells[1].Value = ((nAssembly * 1.0) / nPaint).ToString("0.00 %");
            }
            dgvData.Rows[5].Cells[1].Value = nPackage;
            if(nPackage > 0 && nAssembly > 0)
            {
                dgvData.Rows[6].Cells[1].Value = ((nPackage * 1.0) / nAssembly).ToString("0.00 %");
            }
            dgvData.Rows[7].Cells[1].Value = nPackage;
            if(nPackage > 0 && nMold > 0)
            {
                dgvData.Rows[8].Cells[1].Value = ((nPackage * 1.0) / nMold).ToString("0.00 %");
            }
        }

        /// <summary>
        /// Getting total queue amount
        /// </summary>
        /// <param name="m"></param>
        /// <returns></returns>
        private int GetMessageCount(MessageQueue m)
        {
            Int32 count = 0;
            MessageEnumerator msgEnum = m.GetMessageEnumerator2();
            while (msgEnum.MoveNext(new TimeSpan(0, 0, 0)))
            {
                count++;
            }
            return count;
        }

        /// <summary>
        /// Reading start or stop for the Message Queue 
        /// </summary>
        /// <param name="state"></param>
        private void IsRunning(Boolean state)
        {
            if (state == true)
            {
                btnStart.Enabled = false;
                btnStop.Enabled = true;
                btnSingleRead.Enabled = false;
            }
            else
            {
                btnStart.Enabled = true;
                btnStop.Enabled = false;
                btnSingleRead.Enabled = true;
            }
        }

        private void CboProduct_SelectedIndexChanged(object sender, EventArgs e)
        {
            string selProduct = ((string)cboProduct.SelectedItem).Trim(' ');

            try
            {
                dal.prodCode = Int32.Parse(selProduct.Substring(0, 1));
            }
            catch
            {
                dal.prodCode = 0;
            }

            getData();
        }

        /// <summary>
        /// Start for receiving Message Queue and getting data in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnStart_Click(object sender, EventArgs e)
        {
            if (txtQueueServer.Text == "")
            {
                MessageBox.Show("Message Queue Server required");
            }
            else
            {
                msmq.Path = "Formatname:Direct=os:" + txtQueueServer.Text + queueName;
                bRead = true;
                msmq.BeginReceive();
                IsRunning(true);
            }
        }

        /// <summary>
        /// Stop for receiving Message Queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnStop_Click(object sender, EventArgs e)
        {
            bRead = false;
            IsRunning(false);
        }

        /// <summary>
        /// Clear lstQueueData.Item and YoYoDetail table on Database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnClear_Click(object sender, EventArgs e)
        {
            RemoveData();
            lstQueueData.Items.Clear();
        }

        /// <summary>
        /// Reading single Message Queue
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSingleRead_Click(object sender, EventArgs e)
        {
            if (txtQueueServer.Text == "")
            {
                MessageBox.Show("Message Queue Server required");
            }
            else
            {
                msmq.Path = "Formatname:Direct=os:" + txtQueueServer.Text + queueName;
                try
                {
                    System.Messaging.Message msg = msmq.Receive(new TimeSpan(0));
                    if (msg != null)
                    {
                        lstQueueData.Items.Insert(0, msg.Body.ToString());
                    }
                }
                catch
                {
                    MessageBox.Show("Cannot read - probably empty queue or queue non existent");
                }
            }
        }

        private void btnPurgeQ_Click(object sender, EventArgs e)
        {
            // Delete all messages from the queue.
            msmq.Purge();
        }

        /// <summary>
        /// Setting Gridview
        /// </summary>
        private void Create_GridView()
        {
            DataTable summaryTable = new DataTable("Summary");
            // Add columns to datatable
            DataColumn dc1 = new DataColumn("Title", Type.GetType("System.String"));
            DataColumn dc2 = new DataColumn("Value", Type.GetType("System.String"));
            DataRow dataRecord;

            summaryTable.Columns.Add(dc1);
            summaryTable.Columns.Add(dc2);

            string[] arrDesc = new string[9] {
                "Total parts molded",
                "Total parts successfully molded",
                "Yield at Mold",
                "Total parts successfully painted",
                "Yield at Paint",
                "Total parts successfully assembled",
                "Yield at Assembly",
                "Total parts packaged",
                "Total Yield"
            };

            for (int nCnt = 0; nCnt < arrDesc.Length; nCnt++)
            {
                dataRecord = summaryTable.NewRow();
                dataRecord[0] = arrDesc[nCnt];
                dataRecord[1] = 0;
                summaryTable.Rows.Add(dataRecord);
            }

            dgvData.DataSource = summaryTable;
            dgvData.Columns[0].AutoSizeMode = DataGridViewAutoSizeColumnMode.AllCells;
            dgvData.Refresh();
        }

        private void Create_Chart()
        {
            chartPareto.Series.Clear();

            Series series1 = new Series();
            series1.ChartType = SeriesChartType.Column;
            series1.Name = "Rejected Count";
            series1.Color = System.Drawing.Color.CadetBlue;
            series1.IsVisibleInLegend = true;
            series1.IsXValueIndexed = true;
            series1.IsValueShownAsLabel = true;
            series1.XValueMember = "Reason";
            series1.YValueMembers = "FailCnt";
            chartPareto.Series.Add(series1);

            Series series2 = new Series();
            series2.YAxisType = AxisType.Secondary;
                        
            //series2.ChartType = SeriesChartType.Line;
            series2.ChartType = SeriesChartType.Line;

            series2.Name = "Pareto Line";
            series2.Color = System.Drawing.Color.Red;
            series2.IsVisibleInLegend = true;
            series2.IsXValueIndexed = true;
            series2.IsValueShownAsLabel = true;
            series2.XValueMember = "Reason";
            series2.YValueMembers = "ParetoVal";
            chartPareto.Series.Add(series2);

            //chartPareto.Titles.Add("Pareto Chart for Rejection");

            chartPareto.ChartAreas["ChartArea1"].AxisX.Title = "Reason";
            chartPareto.ChartAreas["ChartArea1"].AxisY.Title = "Amount of Rejection";
            chartPareto.ChartAreas["ChartArea1"].AxisY2.Title = "Average";
            chartPareto.ChartAreas["ChartArea1"].AxisY2.MajorGrid.Enabled = false;
        }
    }
}
