﻿namespace Monitoring
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lstQueueData = new System.Windows.Forms.ListBox();
            this.btnPurgeQ = new System.Windows.Forms.Button();
            this.btnSingleRead = new System.Windows.Forms.Button();
            this.txtRemaining = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.btnClear = new System.Windows.Forms.Button();
            this.txtQueueServer = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.cboProduct = new System.Windows.Forms.ComboBox();
            this.dgvData = new System.Windows.Forms.DataGridView();
            this.btnStop = new System.Windows.Forms.Button();
            this.btnStart = new System.Windows.Forms.Button();
            this.chartPareto = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPareto)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // lstQueueData
            // 
            this.lstQueueData.FormattingEnabled = true;
            this.lstQueueData.ItemHeight = 20;
            this.lstQueueData.Location = new System.Drawing.Point(13, 50);
            this.lstQueueData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.lstQueueData.Name = "lstQueueData";
            this.lstQueueData.Size = new System.Drawing.Size(1160, 184);
            this.lstQueueData.TabIndex = 25;
            // 
            // btnPurgeQ
            // 
            this.btnPurgeQ.Location = new System.Drawing.Point(1061, 6);
            this.btnPurgeQ.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnPurgeQ.Name = "btnPurgeQ";
            this.btnPurgeQ.Size = new System.Drawing.Size(112, 35);
            this.btnPurgeQ.TabIndex = 41;
            this.btnPurgeQ.Text = "Purge Q";
            this.btnPurgeQ.UseVisualStyleBackColor = true;
            this.btnPurgeQ.Click += new System.EventHandler(this.btnPurgeQ_Click);
            // 
            // btnSingleRead
            // 
            this.btnSingleRead.Location = new System.Drawing.Point(939, 6);
            this.btnSingleRead.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnSingleRead.Name = "btnSingleRead";
            this.btnSingleRead.Size = new System.Drawing.Size(112, 35);
            this.btnSingleRead.TabIndex = 40;
            this.btnSingleRead.Text = "Single Read";
            this.btnSingleRead.UseVisualStyleBackColor = true;
            this.btnSingleRead.Click += new System.EventHandler(this.btnSingleRead_Click);
            // 
            // txtRemaining
            // 
            this.txtRemaining.Location = new System.Drawing.Point(610, 6);
            this.txtRemaining.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtRemaining.Name = "txtRemaining";
            this.txtRemaining.Size = new System.Drawing.Size(148, 26);
            this.txtRemaining.TabIndex = 38;
            this.txtRemaining.Text = "0";
            this.txtRemaining.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(519, 11);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(85, 20);
            this.label2.TabIndex = 37;
            this.label2.Text = "Remaining";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(817, 6);
            this.btnClear.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(112, 35);
            this.btnClear.TabIndex = 36;
            this.btnClear.Text = "Clear List";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // txtQueueServer
            // 
            this.txtQueueServer.Location = new System.Drawing.Point(194, 6);
            this.txtQueueServer.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.txtQueueServer.Name = "txtQueueServer";
            this.txtQueueServer.Size = new System.Drawing.Size(258, 26);
            this.txtQueueServer.TabIndex = 35;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 11);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(176, 20);
            this.label1.TabIndex = 34;
            this.label1.Text = "Message Queue Server";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblName.Location = new System.Drawing.Point(1197, 312);
            this.lblName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(129, 26);
            this.lblName.TabIndex = 46;
            this.lblName.Text = "Product Info";
            // 
            // cboProduct
            // 
            this.cboProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cboProduct.FormattingEnabled = true;
            this.cboProduct.Items.AddRange(new object[] {
            "  1. Original Sleeper",
            "  2. Black Beauty",
            "  3. Firecracker",
            "  4. Lemon Yellow",
            "  5. Midnight Blue",
            "  6. Screaming Orange",
            "  7. Gold Glitter",
            "  8. White Lightening",
            "  All Products"});
            this.cboProduct.Location = new System.Drawing.Point(1355, 307);
            this.cboProduct.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.cboProduct.Name = "cboProduct";
            this.cboProduct.Size = new System.Drawing.Size(310, 37);
            this.cboProduct.TabIndex = 45;
            this.cboProduct.Text = "  All Products";
            this.cboProduct.SelectedIndexChanged += new System.EventHandler(this.CboProduct_SelectedIndexChanged);
            // 
            // dgvData
            // 
            this.dgvData.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvData.Location = new System.Drawing.Point(1180, 389);
            this.dgvData.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.dgvData.Name = "dgvData";
            this.dgvData.RowHeadersWidth = 62;
            this.dgvData.Size = new System.Drawing.Size(485, 471);
            this.dgvData.TabIndex = 44;
            // 
            // btnStop
            // 
            this.btnStop.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStop.Location = new System.Drawing.Point(1180, 144);
            this.btnStop.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(488, 129);
            this.btnStop.TabIndex = 43;
            this.btnStop.Text = "Stop Read";
            this.btnStop.UseVisualStyleBackColor = true;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // btnStart
            // 
            this.btnStart.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnStart.Location = new System.Drawing.Point(1180, 6);
            this.btnStart.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(488, 129);
            this.btnStart.TabIndex = 42;
            this.btnStart.Text = "Start Read";
            this.btnStart.UseVisualStyleBackColor = true;
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // chartPareto
            // 
            chartArea1.Name = "ChartArea1";
            this.chartPareto.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chartPareto.Legends.Add(legend1);
            this.chartPareto.Location = new System.Drawing.Point(6, 35);
            this.chartPareto.Name = "chartPareto";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chartPareto.Series.Add(series1);
            this.chartPareto.Size = new System.Drawing.Size(1149, 577);
            this.chartPareto.TabIndex = 47;
            this.chartPareto.Text = "chartPareto";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chartPareto);
            this.groupBox1.Location = new System.Drawing.Point(12, 242);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1161, 618);
            this.groupBox1.TabIndex = 48;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Pareto Chart for Rejection";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1678, 868);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.cboProduct);
            this.Controls.Add(this.dgvData);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnStart);
            this.Controls.Add(this.btnPurgeQ);
            this.Controls.Add(this.btnSingleRead);
            this.Controls.Add(this.txtRemaining);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnClear);
            this.Controls.Add(this.txtQueueServer);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lstQueueData);
            this.MaximizeBox = false;
            this.Name = "frmMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Monitoring";
            this.Load += new System.EventHandler(this.frmMain_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvData)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chartPareto)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox lstQueueData;
        private System.Windows.Forms.Button btnPurgeQ;
        private System.Windows.Forms.Button btnSingleRead;
        private System.Windows.Forms.TextBox txtRemaining;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnClear;
        private System.Windows.Forms.TextBox txtQueueServer;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.ComboBox cboProduct;
        private System.Windows.Forms.DataGridView dgvData;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Button btnStart;
        private System.Windows.Forms.DataVisualization.Charting.Chart chartPareto;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}

