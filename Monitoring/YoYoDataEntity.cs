﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Monitoring
{
    class YoYoDataEntity
    {
        public String WorkArea { get; set; }
        public Guid SerialNumber { get; set; }
        public String Line { get; set; }
        public String State { get; set; }
        public String Reason { get; set; }
        public DateTime DTStamp { get; set; }
        public string DTStamp2 { get; set; }
        public Int32 ProductID { get; set; }

    }
}
