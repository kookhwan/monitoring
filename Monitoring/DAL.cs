﻿//
//  PROG3240 - Business Intelligence
//  Sample solution to Assignment 01
//
//  Author: Kookhwan Im & Dmytro Maherya
//  Date:   4 October 2019
//

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace Monitoring
{
    class DAL
    {
        private SqlConnection conn;
        private SqlCommand cmd;
        public int prodCode = 0;

        public DAL()
        {
            // This connection string may vary from machine to machine.
            conn = new SqlConnection("Data Source=.;Initial Catalog=YoYoData;User ID=sa;Password=Conestoga1");
            cmd = new SqlCommand();
            cmd.Connection = conn;
        }

        /// <summary>
        /// Write to Database
        /// </summary>
        /// <param name="yoyo"></param>
        public void WriteToDB(YoYoDataEntity yoyo)
        {
            String values = "('" + yoyo.WorkArea + "',"
                            + "'" + yoyo.SerialNumber + "',"
                            + "'" + yoyo.Line + "',"
                            + "'" + yoyo.State + "',"
                            + "'" + yoyo.Reason + "',"
                            + "'" + yoyo.DTStamp.ToString() + "',"
                            + yoyo.ProductID.ToString() + ")";

            cmd.CommandText = "INSERT INTO YoYoDetails(WorkArea, SerialNumber, Line, State, Reason, DTStamp, ProductID) VALUES " + values;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Delete All Records
        /// </summary>
        public void DeleteToDB()
        {
            cmd.CommandText = "DELETE FROM YoYoDetails";
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// If not exist YoYoData Table then Create a table
        /// </summary>
        public void CreateTable()
        {
            string strSql;

            strSql = "";
            strSql = strSql + "IF NOT EXISTS (SELECT name FROM sysobjects WHERE name = 'YoYoDetails') ";
            strSql = strSql + "CREATE TABLE YoYoDetails ( ";
            strSql = strSql + "SID INT IDENTITY(1,1) PRIMARY KEY,";
            strSql = strSql + "WorkArea VARCHAR(50) NULL,";
            strSql = strSql + "SerialNumber VARCHAR(50) NULL,";
            strSql = strSql + "Line VARCHAR(10) NULL,";
            strSql = strSql + "State VARCHAR(50) NULL,";
            strSql = strSql + "Reason VARCHAR(50) NULL,";
            strSql = strSql + "DTStamp SMALLDATETIME NULL,";
            strSql = strSql + "ProductID VARCHAR(1) NULL)";

            cmd.CommandText = strSql;
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
        }

        /// <summary>
        /// Query for getting a Rejection count
        /// </summary>
        /// <returns></returns>
        public DataTable GetFailCount()
        {
            string strSql;

            strSql = "";
            strSql = strSql + "SELECT ";
            strSql = strSql + "Pareto, Reason, FailCnt, Average, ";
            strSql = strSql + "ROUND(SUM(Average) OVER(ORDER BY Pareto, FailCnt DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW), 2) AS ParetoVal ";
            strSql = strSql + "FROM ( ";
            strSql = strSql + "SELECT ";
            strSql = strSql + "'1' AS Pareto, Reason, COUNT(*) AS FailCnt, ";
            strSql = strSql + "COUNT(*) * 100.0 / (SELECT Count(*) FROM YoYoDetails WHERE LEN(Reason) > 0 ";

            if (prodCode > 0 && prodCode < 9)
            {
                strSql = strSql + "AND ProductID = " + prodCode + " ";
            }

            strSql = strSql + ") AS Average ";


            strSql = strSql + "FROM YoYoDetails ";
            strSql = strSql + "WHERE LEN(Reason) > 0 ";

            if (prodCode > 0 && prodCode < 9)
            {
                strSql = strSql + "AND ProductID = " + prodCode + " GROUP BY Reason, ProductID) A ";
            }
            else
            {
                strSql = strSql + "GROUP BY Reason ) A ";
            }

            strSql = strSql + "ORDER BY FailCnt DESC";

            SqlDataAdapter adapter = new SqlDataAdapter(@strSql, conn);

            DataTable dt = new DataTable("FailCounts");
            adapter.Fill(dt);

            return dt; 
        }

        /// <summary>
        /// Query for getting a total count
        /// </summary>
        /// <returns></returns>
        public DataTable GetTotalCount()
        {
            string strSql;

            strSql = "";
            strSql = strSql + "SELECT ";
            strSql = strSql + "(SELECT COUNT(*) FROM YoYoDetails WHERE STATE = 'MOLD') AS 'MoldCnt', ";
            strSql = strSql + "(SELECT COUNT(*) FROM YoYoDetails WHERE STATE = 'PAINT') AS 'PaintCnt', ";
            strSql = strSql + "(SELECT COUNT(*) FROM YoYoDetails WHERE STATE = 'ASSEMBLY') AS 'AssembleCnt', ";
            strSql = strSql + "(SELECT COUNT(*) FROM YoYoDetails WHERE STATE = 'PACKAGE') AS 'PackageCnt' ";

            SqlDataAdapter adapter = new SqlDataAdapter(@strSql, conn);

            DataTable dt = new DataTable("FailCounts");
            adapter.Fill(dt);

            return dt;

        }
    }
}
